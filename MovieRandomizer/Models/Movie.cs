﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieRandomizer.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Genre { get; set; }
        public int Runtime { get; set; }
        public int Metascore { get; set; }
        public double ImdbScore { get; set; }
        public int? MyScore { get; set; }
        public DateTime? WatchedDate { get; set; }
        public string Link { get; set; }
        public string Available { get; set; }
    }
}