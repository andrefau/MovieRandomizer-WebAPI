namespace MovieRandomizer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        ReleaseDate = c.DateTime(nullable: false),
                        Genre = c.String(),
                        Runtime = c.Int(nullable: false),
                        Metascore = c.Int(nullable: false),
                        ImdbScore = c.Double(nullable: false),
                        MyScore = c.Int(nullable: false),
                        WatchedDate = c.DateTime(nullable: false),
                        Link = c.String(),
                        Available = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Movies");
        }
    }
}
