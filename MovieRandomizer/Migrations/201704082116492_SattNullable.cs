namespace MovieRandomizer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SattNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Movies", "MyScore", c => c.Int());
            AlterColumn("dbo.Movies", "WatchedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "WatchedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Movies", "MyScore", c => c.Int(nullable: false));
        }
    }
}
