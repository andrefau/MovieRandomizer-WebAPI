namespace MovieRandomizer.Migrations
{
    using MovieRandomizer.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MovieRandomizer.Models.MovieRandomizerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MovieRandomizer.Models.MovieRandomizerContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Movies.AddOrUpdate(x => x.Id,
                new Movie()
                {
                    Id = 1,
                    Title = "The Lion King",
                    ReleaseDate = new DateTime(1994, 8, 18),
                    Genre = "Family",
                    Runtime = 120,
                    Metascore = 80,
                    ImdbScore = 8.1,
                    MyScore = 9,
                    WatchedDate = new DateTime(2017, 4, 8),
                    Link = "Url",
                    Available = "Google Play"
                }
            );
        }
    }
}
